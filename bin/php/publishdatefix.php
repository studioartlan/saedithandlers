<?php

require( 'autoload.php' );

$cli = eZCLI::instance();

$script = eZScript::instance( array(
	'description' => "saedithandlers publishdate",
	'use-session' => true,
	'use-modules' => true,
	'use-extensions' => true,
	'use-debug' => true
) );
$script->startup();

$options = $script->getOptions( "[subtree:]",
				"",
				array(
					'subtree' => 'Node IDs of the subtrees for which to tun the date fix (default: 2)',
				) 
);

$script->initialize();

$subtree = !empty( $options['subtree'] ) ? $options['subtree'] : 2;
 
saIterator::iterateSubtreeArray( $subtree, array(), "saPublishDate::setDateAttributeFromPublished" );

$script->shutdown();

?>