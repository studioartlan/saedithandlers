<?php

$cli = eZCLI::instance();

$script = eZScript::instance( array(
    'description' => "Check if cache should be cleared for scheduled publishing",
    'use-session' => true,
    'use-modules' => true,
    'use-extensions' => true,
    'use-debug' => true,
) );
/*
$requiredParams = array(
    'file-path:' => 'The path to the file that will be generated. It will be generated in "export" subfolder of current site\'s storage folder',
    'generator:' => 'The identifier of the generator that will be used to generate the contents of the file (corresponds to group in sageneratefile.ini)',
);

$optionalParams = array(
);
$options = saImport::parseParams( $requiredParams, $optionalParams );
*/

$sys = eZSys::instance();
$script->startup();
$script->initialize();

saPublishDate::processCachePendingActions();

?>
