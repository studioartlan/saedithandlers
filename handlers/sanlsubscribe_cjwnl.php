<?php

class saNLSubscribe_CJWNL extends saNLSubscribe
{

	static function SetSubscription( $email, $first_name, $last_name, $listIdentifier, $user_object_id = false, $unsubscribe = false, $unsubscribe_all = false)
	{

		$context = 'subscribe';
		$subscriptionDataArray = array(
			'salutation'=> -1,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'id_array' => array( $listIdentifier ),
			'list_array' => array( $listIdentifier ),
			'list_output_format_array' => array( $listIdentifier => array( 0 ) ),
			'ez_user_id' => $user_object_id
		);

#print_r( $subscriptionDataArray );

		$subscriptionResultArray = CjwNewsletterSubscription::createSubscriptionByArray(
			$subscriptionDataArray,
			CjwNewsletterUser::STATUS_CONFIRMED,
			true,
			$context
		);

#print_r( $subscriptionResultArray );
#exit;
		return $subscriptionDataArray;

	}

	
}
		
?>
