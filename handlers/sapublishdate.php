<?PHP
/*

    saEditHandlers
    Copyright (C) 2010 Studio Artlan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	For any questions contact xmak@studioartlan.com.
	
*/

class saPublishDate
{

	static $debugOn = null;
	static $cachePendingActionName = 'sapublishdate_clearcache';

	static function processCachePendingActions()
	{
		echo "Checking cache clear for sapublishdate...\n";
		$actions = eZPendingActions::fetchByAction(self::$cachePendingActionName);
		$currentTime = time();

		$shouldClearCache = false;

		echo "Found " . count($actions) . " actions...\n";

		foreach ($actions as $action)
		{
			$publishDate = $action->param;

			echo "Cache clear action: $publishDate (" . date("d.m.Y. H.i:s", $publishDate) . ")\n";

			if ($publishDate <= $currentTime)
			{
				echo "Found publishdate past due: $publishDate (" . date("d.m.Y. H.i:s", $publishDate) . ")\n";
				$shouldClearCache = true;
				$action->remove();
			}

		}

		if ($shouldClearCache)
		{
			echo "Clearing cache...\n";

			#$cacheList = eZCache::fetchList();
			#$cacheEntries = eZCache::fetchByTag( 'content', $cacheList );

			$cacheEntries = eZCache::fetchByIdList( array('content', 'template-block') );
			foreach ( $cacheEntries as $cacheEntry )
			{
				echo "Clearing cache entry $cacheEntry[name] (id=$cacheEntry[id], path=$cacheEntry[path]) \n";
				eZCache::clearItem( $cacheEntry );
			}

			echo "Clear cache DONE.\n";

			echo "Clearing CF cache (ALL)...\n";

			$cf = new saCloudFlare();
		    $result = $cf->clearZoneCacheAll();

			$success = $result['success'];

			if ($success)
				echo "Clear CF cache SUCCESS: " . json_encode($result) . "\n";
			else
				echo "Clear CF cache FAILED: " . json_encode($result) . "\n";

		}
		else
		{
			echo "No actions require cache clear.\n";
		}

	}

	static function SetPublishDate( $contentObjectID, $contentObjectVersion )
	{

		$object = eZContentObject::fetch( $contentObjectID );
		$modifyDate = false;
		
		if ( !$object )
		{
			self::DebugError( "Object with ID $contentObjectID doesn't exsist" );
			return false;
		}
		
		$classID = $object->attribute( 'contentclass_id' );
		$class = eZContentClass::fetch( $classID );
		$classIdentifier = $class->attribute( 'identifier' );
		
		$publishdateINI = eZINI::instance( 'sapublishdate.ini' );
		
		if ( !$publishdateINI )
		{
			self::DebugError( "No INI file." );
			return false;
		}

		$useAllClases = $publishdateINI->variable( 'PublishDateSettings', 'UseAllClasses' ) == 'true';
		if ( $useAllClases )
			$modifyDate = true; 
		else
		{
			if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DateClasses' ) )
				$object_classes = $publishdateINI->variable( 'PublishDateSettings', 'DateClasses' );
			else
				$object_classes = array();
			$modifyDate = $useAllClases || in_array($classIdentifier, $object_classes);
		}

		if ( !$modifyDate )
		{
			return true;
		}
		
		if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DateAttributes' ) )
			$dateAttributes = $publishdateINI->variable( 'PublishDateSettings', 'DateAttributes' );
		else
			$dateAttributes = array();
		
		if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DefaultDateAttribute' ) )
			$defaultDateAttribute = $publishdateINI->variable( 'PublishDateSettings', 'DefaultDateAttribute' );
		else 
			$defaultDateAttribute = '';
			 
		if (isset($dateAttributes[$classIdentifier]))
			$dateAttributeName = $dateAttributes[$classIdentifier];
		else
			$dateAttributeName = $defaultDateAttribute;

		if ( !$dateAttributeName )
		{
			return true;
		}
		
		$dataMap = $object->DataMap();

		if ( empty( $dataMap[$dateAttributeName] ) )
		{
			self::DebugWarning( "Attribute $dateAttributeName doesn't exist in object/class: $contentObjectID/$classIdentifier." );
			return false;
		}
		
		$dateAttribute = $dataMap[$dateAttributeName];

		$publishedDate = false;

		if ( $dateAttribute->attribute( 'has_content' ) )
		{
			$publishedDate = $dateAttribute->content()->DateTime;
		}
		else
		{
			if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DefaultFillEmptyAttribute' ) )
				$defaultFillEmptyAttribute = $publishdateINI->variable( 'PublishDateSettings', 'DefaultFillEmptyAttribute' );
			else
				$defaultFillEmptyAttribute = false;
			
			if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'FillEmptyAttributes' ) )
				$fillEmptyAttributes = $publishdateINI->variable( 'PublishDateSettings', 'FillEmptyAttributes' );
			else
				$fillEmptyAttributes = array();
				 
			$doFillEmptyAttribute = ($defaultFillEmptyAttribute  == 'true');

			if (isset($fillEmptyAttributes[$classIdentifier]))
			{
				$doFillEmptyAttribute = ($fillEmptyAttributes[$classIdentifier] == 'true');
			}

			if ($doFillEmptyAttribute)
			{
				// TODO: Staviti ovdje da uzima datum objave, a ne trenutno vrijeme

				$publishedDate = time();
				$dateAttribute->setAttribute('data_int', $publishedDate );
				$dateAttribute->store();
				$object->store();
			}
		}

		if ($publishedDate)
		{
			$object->setAttribute( 'published', $publishedDate);
			$object->store();

			$action = new eZPendingActions();
			$action->setAttribute('action', self::$cachePendingActionName);
			$action->setAttribute('created', time());
			$action->setAttribute('param', $publishedDate);
			$action->store();

		}

	}



	static function setDateAttributeFromPublished( $node )
	{

		$object = $node->attribute( 'object' );
		
		$classID = $object->attribute( 'contentclass_id' );
		$class = eZContentClass::fetch( $classID );
		$classIdentifier = $class->attribute( 'identifier' );
		
		$publishdateINI = eZINI::instance( 'sapublishdate.ini' );
		
		if ( !$publishdateINI )
		{
			self::DebugError( "No INI file." );
			return false;
		}

		if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DateAttributes' ) )
			$dateAttributes = $publishdateINI->variable( 'PublishDateSettings', 'DateAttributes' );
		else
			$dateAttributes = array();
		
		if ( $publishdateINI->hasVariable( 'PublishDateSettings', 'DefaultDateAttribute' ) )
			$defaultDateAttribute = $publishdateINI->variable( 'PublishDateSettings', 'DefaultDateAttribute' );
		else 
			$defaultDateAttribute = '';
			 
		if ( isset($dateAttributes[$classIdentifier]) )
			$dateAttributeName = $dateAttributes[$classIdentifier];
		else
			$dateAttributeName = $defaultDateAttribute;

		if ( !$dateAttributeName )
		{
			return true;
		}
		
		$dataMap = $object->DataMap();

		if ( empty( $dataMap[$dateAttributeName] ) )
		{
			self::DebugWarning( "Attribute $dateAttributeName doesn't exist in object/class: $object->ID/$classIdentifier." );
			return false;
		}
		
		$dateAttribute = $dataMap[$dateAttributeName];

		if ( !$dateAttribute->attribute( 'has_content' ) )
		{
			$publishedDate = $object->attribute( 'published' );
			$dateAttribute->fromString( $publishedDate );
			$dateAttribute->store(); 
		}

	}

	
	static function DebugError($msg)
	{
		if ( self::debugOn() )		
			eZDebug::writeError( $msg, "sapublishdate" );
	}

	static function DebugWarning($msg)
	{
		if ( self::debugOn() )		
			eZDebug::writeWarning( $msg, "sapublishdate" );
	}

	static function DebugNotice($msg)
	{
		if ( self::debugOn() )		
			eZDebug::writeNotice( $msg, "sapublishdate" );
	}
	
	static function debugOn()
	{
		if ( self::$debugOn === null )
		{
			self::$debugOn = false;
			$ini = eZINI::instance( 'saedithandlers.ini' );
		
			if ( $ini->hasVariable( 'DebugSettings', 'DebugOn' ) )
				self::$debugOn = $ini->variable( 'DebugSettings', 'DebugOn' ) === 'true';
		} 
		
		return self::$debugOn;
		
	}

}
		
?>
